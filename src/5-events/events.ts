import {html, LitElement} from "lit";
import {customElement, property, eventOptions} from "lit/decorators.js";

@customElement('element-event')
export class ElementEvent extends LitElement {
    @property({type: Number})
    count = 0;
    @property() hostName = '';
    @property() shadowName = '';

    constructor() {
        super();
        //The component constructor is a good place to add event listeners on the component.
        // this.addEventListener('click',
        //     (e: Event) => this.hostName = (e.target as Element).localName);

        //create event
        // const myEvent = new Event('my-event', {bubbles: true, composed: true}); //composed option is useful to set to allow the event to be dispatched above the shadow DOM tree in which the element exists.
        // this.addEventListener('my-event', () => console.log("called from new event"));
        // this.dispatchEvent(myEvent);
        // //with custom event you can add more data to the event
        // const myCustomEvent = new CustomEvent('my-custom-event',{detail:this.count});
        // this.addEventListener('my-custom-event', (e:CustomEvent)=>{console.log(e.detail)})
        // this.dispatchEvent(myCustomEvent);
    }

    protected override createRenderRoot() {
        //events fired from the component's shadow DOM are retargeted when heard by an event listener on the component.
        //return the shadow root from the createRenderRoot method
        const root = super.createRenderRoot();
        root.addEventListener('click',
            (e: Event) => this.shadowName = (e.target as Element).localName);
        return root;
    }

    // override async firstUpdated() {
    //     // fires after the first time your component has been updated and called its render method, but before the browser has had a chance to paint.
    //     console.log("first updated called")
    //     // Give the browser a chance to paint
    //     await new Promise((r) => setTimeout(r, 0));
    //     this.addEventListener('click', this._handleClick);
    // }
    // _handleClick(e) {
    //     console.log(e.target.localName)
    //    // console.log(this.prop);
    // }

    @eventOptions({passive: true}) //provide event options
    _touchStart() {

    }

    private _increment() {
        this.count++;
    }


    protected override render() {
        return html`
            <p>${this.count}</p>
            <button @click="${this._increment}">Increment</button>
            <hr/>
            <p>Shadow dom events</p>
            <button>Click Me!</button>
            <p>Component target: ${this.hostName}</p>
            <p>Shadow target: ${this.shadowName}</p>
            <hr/>
        `
    }
}