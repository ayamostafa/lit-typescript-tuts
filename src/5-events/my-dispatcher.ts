import {customElement, property} from "lit/decorators.js";
import {html, LitElement} from "lit";

@customElement('my-dispatcher')

export class MyDispatcher extends LitElement {
    @property({type: Boolean}) open = true;

    override render() {
        return html`
            <span>Hello from dispatcher</span>
            <button @click="${this._notify}">${this.open ? 'Close' : 'Open'}</button>
            <p ?hidden="${this.open}">Content</p>
        `
    }

    async _notify() {
        this.open = !this.open;
        await this.updateComplete;
        const name = this.open ? 'opened' : 'closed';
        this.dispatchEvent(new CustomEvent(name, {
            bubbles: true,
            composed: true,
            detail: {userData: {name: 'aya', address: 'cairo'}}
        }));
        // this.dispatchEvent(new CustomEvent('opened', {bubbles: true, composed: true}));
    }
}