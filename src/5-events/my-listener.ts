import {customElement, property} from "lit/decorators.js";
import {html, LitElement} from "lit";

@customElement('my-listener')

export class MyListener extends LitElement {
    @property({type: Number}) height: number | null = null;

    override render() {
        return html`
            <spa>hello from listener</spa>
            <p @opened="${this._listener}" @closed="${this._listener}"><slot></slot></p>
        <p>Height: ${this.height}px</p>
        `
    }
    protected override updated() {
        if (this.height === null) {
            requestAnimationFrame(() => this.height = this.getBoundingClientRect().height);
        }
    }
    _listener(e:CustomEvent){
        this.height = null;
         console.log(e.detail.userData);
    }
}