/**
 * @license
 * Copyright 2019 Google LLC
 * SPDX-License-Identifier: BSD-3-Clause
 */

import {LitElement, html, css, PropertyValues} from 'lit';
import {customElement, property, queryAssignedNodes, queryAssignedElements} from 'lit/decorators.js';

/**
 * An example element.
 *
 * @fires count-changed - Indicates when the count changes
 * @slot - This element has a slot
 * @csspart button - The button
 */
@customElement('my-element')
export class MyElement extends LitElement {
    static override styles = css`
      :host {
        display: block;
        border: solid 1px red;
        padding: 16px;
        max-width: 800px;
      }
    `;

    /**
     * The name to say "Hello" to.
     */
    @property()
    name = 'World';

    /**
     * The number of times the button has been clicked.
     */
    @property({type: Number})
    count = 0;

    get _slottedChildren() {
        const slot = this.shadowRoot?.querySelector('slot');
        return slot?.assignedElements({flatten: true});
    }

    @queryAssignedElements({slot: 'list', selector: '.item'})
    _listItems!: Array<HTMLElement>;

    @queryAssignedNodes({slot: 'header', flatten: true})
    _headerNodes!: Array<Node>;

    //The examples above are equivalent to the following code:

    // get _listItems() {
    //     const slot = this.shadowRoot?.querySelector('slot[name=list]');
    //     return slot?.assignedElements().filter((node) => node.matches('.item'));
    // }
    //
    // get _headerNodes() {
    //     const slot = this.shadowRoot?.querySelector('slot[name=header]');
    //     return slot?.assignedNodes({flatten: true});
    // }

    protected override firstUpdated(_changedProperties: PropertyValues) {
        console.log("_slottedChildren")
        console.log(this._slottedChildren);
        return super.firstUpdated(_changedProperties);
    }

    handleSlotchange(e) {
        const childNodes = e.target.assignedNodes({flatten: true, slot: "one", selector: 'h1'});
        // ... do something with childNodes ...
        childNodes.map((node) => {
            console.log(node.innerText);
            return node.textContent ? node.textContent : ''
        }).join('');
    }

    override render() {
        // this._headerNodes.then(node => console.log(node));
        return html`
            <h1>${this.sayHello(this.name)}!</h1>
            <button @click=${this._onClick} part="button">
                Click Count: ${this.count}
            </button>
            <slot>I'm fallback content</slot>
            <slot @slotchange=${this.handleSlotchange} name="one"></slot>
            <slot name="nope"></slot>
        `;
    }

    private _onClick() {
        this.count++;
        this.dispatchEvent(new CustomEvent('count-changed'));
    }

    /**
     * Formats a greeting
     * @param name The name to say "Hello" to
     */
    sayHello(name: string): string {
        return `Hello, ${name}`;
    }
}

declare global {
    interface HTMLElementTagNameMap {
        'my-element': MyElement;
    }
}
