import {LitElement, html, css, PropertyValues} from "lit";
import {customElement, property , query, queryAll, queryAsync} from "lit/decorators.js";

@customElement('shadow-dom')
class Shadowdom extends LitElement {
    @property()
    name = 'aya';

    get _first() {
        return this.renderRoot?.querySelector("#first") ?? null;
    }

    //instead we could use query
    @query("#first")
    _first;
    // like query except that it returns a Promise that resolves to that node after any pending element render is completed.
    @queryAsync("#first")
    _firstWithPromise;
    //the same for querySelectorAll there's queryAll
    @queryAll("div")
    _div;

    protected override firstUpdated(_changedProperties: PropertyValues) {
        // console.log("_changedProperties")
        // console.log(_changedProperties);
        console.log("first")
        console.log(this._first);
        console.log("_firstWithPromise");
       // console.log(this._div)
        return super.firstUpdated(_changedProperties);
    }

    override render() {
        this._firstWithPromise.then(elem=>{console.log("inside promise result");console.log(elem)});

        return html`
            <h1>Hello From ShadowDom ${JSON.stringify(this._first)}</h1>
            <div id="first">First Div ${this.name}</div>
            <div id="second">Second Div</div>
        `;
    }
}