import {Directive, directive} from 'lit/directive.js'
import {html} from "lit";
import {noChange} from 'lit';

class HelloCustomDirective extends Directive {
    // Differences between update() and render()
    // directives should return values from render() and only use update() for logic that requires access to the DOM.

    // You must implement the render() callback for all directives.
    override render() {
        return `Hello`;
        //or doesn't return any thing
       // return noChange so the dom doesn't need to update;
    }
}

const hello = directive(HelloCustomDirective);

const template = html`
    <div>${hello()}</div>`;
