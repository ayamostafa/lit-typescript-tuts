import {directive} from 'lit/directive.js';
import {AsyncDirective} from 'lit/async-directive.js';
import {noChange} from 'lit';
import {Observable, Subscription} from "rxjs";

class ObserveDirective extends AsyncDirective {
    observable: Observable<unknown> | undefined;
    unsubscribe: Subscription;
    // When the observable changes, unsubscribe to the old one and
    // subscribe to the new one
    render(observable: Observable<unknown>) {
        if (this.observable !== observable) {
            this.unsubscribe.unsubscribe();
            this.observable = observable
            if (this.isConnected) {
                this.subscribe(observable);
            }
        }
        return noChange;
    }

    // Subscribes to the observable, calling the directive's asynchronous
    // setValue API each time the value changes
    subscribe(observable: Observable<unknown>) {
        this.unsubscribe = observable.subscribe((v: unknown) => {
            this.setValue(v);
        });
    }

    // When the directive is disconnected from the DOM, unsubscribe to ensure
    // the directive instance can be garbage collected
    override disconnected() {
        this.unsubscribe.unsubscribe();
    }

    // If the subtree the directive is in was disconnected and subsequently
    // re-connected, re-subscribe to make the directive operable again
    override reconnected() {
        this.subscribe(this.observable!);
    }
}

export const observe = directive(ObserveDirective);