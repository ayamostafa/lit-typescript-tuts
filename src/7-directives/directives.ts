import {customElement, property, state} from "lit/decorators.js";
import {html, LitElement} from "lit";
import {repeat} from 'lit/directives/repeat.js';
import {classMap} from 'lit/directives/class-map.js';
import {styleMap} from 'lit/directives/style-map.js';
import {when} from 'lit/directives/when.js';
import {choose} from 'lit/directives/choose.js';
import {join} from 'lit/directives/join.js';
import {map} from 'lit/directives/map.js';
import {range} from 'lit/directives/range.js';
import {guard} from 'lit/directives/guard.js';
import {live} from 'lit/directives/live.js';
import {ref, createRef, Ref} from 'lit/directives/ref.js';
import {until} from 'lit/directives/until.js';
import {asyncAppend} from 'lit/directives/async-append.js';
import {asyncReplace} from 'lit/directives/async-replace.js';

async function* countDown(count: number) {
    while (count > 0) {
        yield count--;
        await new Promise((r) => setTimeout(r, 1000));
    }
}

async function* tossCoins(count: number) {
    for (let i = 0; i < count; i++) {
        yield Math.random() > 0.5 ? 'Heads' : 'Tails';
        await new Promise((r) => setTimeout(r, 1000));
    }
}


@customElement('my-directive')
class MyDirective extends LitElement {
    @state()
    private content = fetch('content.txt').then(r => r.text());
    @state()
    private tosses = tossCoins(10);
    @state()
    private timer = countDown(10);
    inputRef: Ref<HTMLInputElement> = createRef();

    private sort = 1;
    @property()
    enabled = false;
    @property()
    value: string = '';
    @property() employees = [
        {id: 0, givenName: 'Fred', familyName: 'Flintstone'},
        {id: 1, givenName: 'George', familyName: 'Jetson'},
        {id: 2, givenName: 'Barney', familyName: 'Rubble'},
        {id: 3, givenName: 'Cosmo', familyName: 'Spacely'}
    ];

    private _calculateSHA() {
    }

    private toggleSort() {
        this.sort *= -1;
        this.employees = [...this.employees.sort((a, b) =>
            this.sort * (a.familyName.localeCompare(b.familyName) ||
            a.givenName.localeCompare(b.givenName)))];
    }

    override render() {
        const classes = {enabled: this.enabled, hidden: true};
        const styles = {backgroundColor: this.enabled ? 'blue' : 'gray', color: 'white', '--custom-color': 'steelblue'};

        return html`
            <hr/>
            <h1>Directives</h1>
            <h4>Repeat</h4>
            <ul>
                <!--  The difference between repeat and map that map maintain dom nodes and reassign the values but repeat reorder the existing dom-->
                <!--  repeat(items, keyFunction, itemTemplate) keyFunction is a function that takes a single item as an argument and returns a guaranteed unique key for that item.
                itemTemplate is a template function that takes the item and its current index as arguments, and returns a TemplateResult.-->
                <!--//when to use repeat-->
                <!--If updating the DOM nodes is more expensive than moving them, use the repeat directive.-->
                <!--If the DOM nodes have state that isn't controlled by a template expression, use the repeat directive.-->

                ${repeat(this.employees, (employee) => employee.id, (employee, index) => html`
                    <li>${index}: ${employee.familyName}, ${employee.givenName}</li>
                `)}
            </ul>
            <button @click=${this.toggleSort}>Toggle sort</button>
            <h4>Class Map</h4>
            <div class=${classMap(classes)}>Classy text</div>
            <h4>Style Map</h4>
            <p style=${styleMap(styles)}>Hello style!</p>
            <h4>When</h4>
            ${when(this.enabled, () => html`User: Aya`, () => html`Sign In...`)}
            ${choose(this.enabled, [
                        [true, () => html`<h1>Home</h1>`],
                        [false, () => html`<h1>About</h1>`]
                    ],
                    () => html`<h1>Error</h1>`)}
            <h4>Join</h4>
            <!--   Returns an iterable containing the values in items interleaved with the joiner value.-->
            ${join(
                    map(this.employees, (i) => html`<a href=${i.id}>${i.familyName}</a>`),
                    html`<span class="separator">|</span>`
            )}
            <h4>Range</h4>
            <!-- Returns an iterable of integers from start to end (exclusive) incrementing by step.-->
            ${map(range(8), (i) => html`${i + 1}`)}
            <!--cache-->
            <!--The cache directive caches the generated DOM for a given expression and input template. In the example above, it caches the DOM for both the summaryView and detailView templates. When you switch from one view to another, Lit swaps in the cached version of the new view and updates it with the latest data. This can improve rendering performance when these views are frequently switched.-->
            <h4>Guard</h4>
            <!--  In this case, the expensive calculateSHA function is only run when the value property changes.-->
            ${guard([this.value], () => this._calculateSHA())}

            <h4>Live</h4>
            <!-- When determining whether to update the value, checks the expression value against the live DOM value, instead of Lit's default behavior of checking against the last set value.-->
            <!-- In these cases if the DOM value changes, but the value set through Lit expression hasn't, Lit won't know to update the DOM value and will leave it alone. If this is not what you want—if you want to overwrite the DOM value with the bound value no matter what—use the live() directive.-->
            <input .value=${live(this.employees[0].familyName)}>
            <h4>Ref</h4>
            <input ${ref(this.inputRef)}>
            <h4>Until</h4>
            ${until(this.content, html`<span>Loading...</span>`)}
            <h4>asyncAppend</h4>
            <ul>${asyncAppend(this.tosses, (v: string) => html`
                <li>${v}</li>`)}
            </ul>
            <h4>asyncReplace</h4>
            Timer: <span>${asyncReplace(this.timer)}</span>.
        `;

    }
}