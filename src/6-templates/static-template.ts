import {LitElement} from 'lit';
import {customElement, property} from 'lit/decorators.js';
import {html, literal, unsafeStatic} from 'lit/static-html.js';

@customElement('my-button')
class MyButton extends LitElement {
    tag = literal`button`;
    activeAttribute = literal`active`;
    @property() caption = 'Hello static';
    @property({type: Boolean}) active = false;

    tagWithUnsafeStatic = 'button';
    activeAttributeWithUnsafeStatic = 'active';

    override render() {
        // <!-- *****************Important Note *************-->
        // Changing the value of static expressions is expensive. Expressions using literal values should not change frequently, as they cause a new template to be re-parsed and each variation is held in memory.
        return html`
            <hr/>
            <h4>Static with literal</h4>
            <${this.tag} ${this.activeAttribute}=${this.active}>
                <p>${this.caption}</p>
            </${this.tag}>
        <h4>Static with unsafeStatic</h4>
            <${unsafeStatic(this.tagWithUnsafeStatic)} ${unsafeStatic(this.activeAttributeWithUnsafeStatic)}=${this.active}>
        <p>${this.caption}</p>
      </${unsafeStatic(this.tagWithUnsafeStatic)}>
        `;
    }
}