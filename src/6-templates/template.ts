import {customElement, property, query} from "lit/decorators.js";
import {html, LitElement} from "lit";
import {ifDefined} from 'lit/directives/if-defined.js';
import {cache} from 'lit/directives/cache.js';


@customElement('my-template')
class MyTemplate extends LitElement {
    @property({type: Boolean})
    show: boolean = true;
    @property()
    todos = ['todo1', 'todo2', 'todo3'];
    @property()
    imagePath = 'img.jpg'
    @query('p[title="todo1"]')
    para;
    nav = html`
        <nav>This is navbar</nav>`;
    div = document.createElement('div');
    img = html`<img width="50" src="/dev/images/${ifDefined(this.imagePath)}">`;


    override updated() {
        console.log(this.para)
    }

    getUserMsg() {
        if (this.show) {
            return html`Welcome User`;
        }
        return html`Not Found`;
    }

    override render() {
        let message;
        if (false) {
            message = html`Welcome Aya`;
        } else {
            message = html`Change Show Boolean Value
            <button @click="${() => {
                this.show = !this.show;
                console.log("changed show flag" + this.show)
            }}">Login
            </button>`;
        }
        // Iterable elements
        const itemTemplates = [];
        for (const i of this.todos) {
            itemTemplates.push(html`
                <li>${i}</li>`);
        }
        return html`
            <h4>Conditional Expression inside render ${message}</h4>
            <!-- The cache directive caches DOM for templates that aren't being rendered currently.-->
            <h4>Conditional Expression with calling function ${cache(this.getUserMsg())}</h4>
            <hr/>
            <ul>
                ${itemTemplates}
            </ul>
            ${this.img}
            ${this.nav}
            ${this.div}
            <p .title=${this.todos[0]}>set a JavaScript property on an element using the . prefix and the property
                name:</p>
            <p ?hidden=${!this.show}>To set a boolean attribute, use the ? prefix</p>
            ${this.show ? html`
                <ul>${this.todos.map(i => html`
                    <li>${i}</li>`)}
                </ul>
            ` : ''}
            <hr/>
            <!-- ERROR This will throw error as dynamic content isn't allowed inside template -->
            <!-- <template>${this.todos[0]}</template> -->

            <!-- OK -->
            <template id="${this.todos[0]}">static content ok</template>
            <hr/>
            <!-- BEWARE Expressions are not supported inside textarea
            // <textarea>${this.todos[0]}</textarea>
            Instead add value property -->
            <textarea .value=${this.todos[0]}></textarea>
            <!-- Similarly, inside elements with the contenteditable attribute. Instead, bind to the .innerText property of the element. -->
            <!-- BEWARE
            <div contenteditable>${this.todos[0]}</div> -->

            <!-- OK -->
            <div contenteditable .innerText=${this.todos[0]}></div>

            <!-- OK -->
            <div contenteditable id="${this.todos[0]}">static content ok</div>
        `
    }
}