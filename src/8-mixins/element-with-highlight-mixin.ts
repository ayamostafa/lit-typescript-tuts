import {LitElement, html,css} from 'lit';
import {customElement} from 'lit/decorators.js';
import {HighlightableMixin} from './highlightable-mixin.js'
const HighlightableLitElement = HighlightableMixin(LitElement);

@customElement('element-with-highlight-mixin')
export class ElementOne extends HighlightableLitElement {
    static styles = [
        HighlightableLitElement.styles || [],
        css`:host { display: block; }`
    ];
    render() {
        return html`
            <hr/>
            <p>Render Element with highlight property</p>
            ${this.renderHighlight(html`<p>Simple highlight</p>`)}

            <p>Change highlight property from element </p>
            ${this.renderHighlight(html`
                <label>
                    <input type="checkbox"
                           .checked=${this.highlight}
                           @change=${this.toggleHighlight}>
                    Toggleable highlight
                </label>
            `)}
        `
    }
    private toggleHighlight(event: Event) {
        this.highlight = (event.target as HTMLInputElement).checked;
    }
}
