import {LitElement, html} from 'lit';
import {customElement} from 'lit/decorators.js';
import LoggingMixin from '../../scripts/8-composition/logging-mixin.js'

@customElement('element-with-logging-mixin')
export class ElementOne extends LoggingMixin(LitElement) {
    render(){
        return html`
            <hr/>
            <h4>Mixins</h4>
            <p>Hello from element-with-logging-mixin</p>
        `
       // return this.renderHighlight(html`<p>Simple highlight</p>`);
    }
}