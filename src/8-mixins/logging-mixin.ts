const LoggingMixin = (superClass:any) => class extends superClass {
    constructor() {
        super();
        console.log(`${this.localName} was created`);
    }
    connectedCallback() {
        super.connectedCallback();
        console.log(`${this.localName} was connected`);
    }
    updated(changedProperties:any) {
        super.updated?.(changedProperties);
        console.log(`${this.localName} was updated`);
    }
}
export default LoggingMixin;