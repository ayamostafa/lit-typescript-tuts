import {html,css, LitElement} from "lit";
import {property} from "lit/decorators.js";
import {classMap} from "lit/directives/class-map.js";
type Constructor<T> = new (...args: any[]) => T;

export declare class HighlightableInterface {
    highlight: boolean;
    protected renderHighlight(content: unknown): unknown;
}

export const HighlightableMixin =
    <T extends Constructor<LitElement>(superClass: T) => {
        class HighlightableElement extends superClass {
            // Adds some styles...
            static styles = [
                (superClass as unknown as typeof LitElement).styles ?? [],
                css`.highlight { background: yellow; }`
            ];

            // ...a public `highlight` property/attribute...
            @property({type: Boolean}) highlight = false;

            // ...and a helper render method:
            renderHighlight(content: unknown) {
                return html`
          <div class=${classMap({highlight: this.highlight})}>
            ${content}
          </div>`;
            }
        }
        return HighlightableElement as Constructor<HighlightableInterface> & T;
    };