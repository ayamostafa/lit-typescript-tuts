/**
 * @license
 * Copyright 2020 Google LLC
 * SPDX-License-Identifier: BSD-3-Clause
 */

import {configureLocalization} from '@lit/localize';
import {sourceLocale, targetLocales} from '../../scripts/locales/generated/locale-codes.js';

export const {getLocale, setLocale} = configureLocalization({
    sourceLocale,
    targetLocales,
    loadLocale: (locale: string) => import(`/scripts/locales/generated/${locale}.js`), //lazy load

});

export const setLocaleFromUrl = async () => {
    const url = new URL(window.location.href);
    const locale = url.searchParams.get('locale') || sourceLocale;
    await setLocale(locale);
};