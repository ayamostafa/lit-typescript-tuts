import {html, LitElement} from 'lit';
import {customElement, property} from 'lit/decorators.js';
import {localized, msg, str, updateWhenLocaleChanges} from '@lit/localize';
import {allLocales} from '../../scripts/locales/generated/locale-codes.js';
import {getLocale, setLocaleFromUrl} from '../../scripts/10-localization/localization.js';

const localeNames: {
    [L in typeof allLocales[number]]: string;
} = {
    en: 'English',
    'es-419': 'Español (Latinoamérica)‎',
    'ar': 'Arabic',
};

@localized() //run time mode
@customElement('localized-element')
class LocalizedElement extends LitElement {

    constructor() {
        super();
        updateWhenLocaleChanges(this); //should add this so locale changes applied
        // Show/hide a progress indicator whenever a new locale is loading,
// and re-render the application every time a new locale successfully loads.
    }

    //tip performance
    // Don't do this! There's no reason to include the <button> tag in this
    // localized template.
    // return msg(html`<button>Launch rocket</button>`);
    //*****instead
    // return html`<button>${msg('Launch rocket')}</button>`;

    protected override createRenderRoot() {
        //events fired from the component's shadow DOM are retargeted when heard by an event listener on the component.
        //return the shadow root from the createRenderRoot method
        const root = super.createRenderRoot();
        window.addEventListener('lit-localize-status', (event) => {
            const spinner = root.querySelector('#spinner');
            console.log(spinner);
            if (event.detail.status === 'loading') {
                console.log(`Loading new locale: ${event.detail.loadingLocale}`);
                spinner?.removeAttribute('hidden');
                console.log(spinner)
            } else if (event.detail.status === 'ready') {
                console.log(`Loaded new locale: ${event.detail.readyLocale}`);
                spinner?.setAttribute('hidden', '');
            } else if (event.detail.status === 'error') {
                console.error(
                    `Error loading locale ${event.detail.errorLocale}: ` +
                    event.detail.errorMessage
                );
                spinner?.setAttribute('hidden', '');
            }
        });

        return root;
    }

    @property()
    who = 'aya';

    override render() {
        return html`
            <hr/>
            <h4>Localization</h4>
            <div id="spinner">Loading.....................</div>
            <select @change=${this.localeChanged}>
                ${allLocales.map(
                        (locale: any) =>
                                html`
                                    <option value=${locale} ?selected=${locale === getLocale()}>
                                        ${localeNames[locale]}
                                    </option>`
                )}
            </select>
            Hello <b>${msg(html`Hello <b>World</b>!`)}</b>
            <!--you can override the id for msg-->
            Hello <b>${msg(str`Hello ${this.who}`, {id: 'custom-id'})}</b>
            <!--  Use the desc option to the msg function to provide human-readable descriptions for your strings and templates. These descriptions are shown to translators by most translation tools, and are highly recommended to help explain and contextualize the meaning of messages.-->
            Hello <b>${msg("Launch", {
                desc: "Button that begins rocket launch sequence.",
            })}</b>

        `;
    }

    localeChanged(event: Event) {
        const newLocale = (event.target as HTMLSelectElement).value;
        if (newLocale !== getLocale()) {
            const url = new URL(window.location.href);
            url.searchParams.set('locale', newLocale);
            window.history.pushState(null, '', url.toString());
            setLocaleFromUrl();
        }
    }
}