import {LitElement, html, css} from "lit";
import {customElement, property, state} from "lit/decorators.js";

@customElement('my-3-props')
export class MyProps extends LitElement {
    static styles = css`
      :host {
        display: inline-block;
      }

      :host([active]) {
        border: 1px solid red;
      }
    `;
    @property({ attribute: 'my-name' })
    myName = 'Ogden';
    @state()
    protected _active = false;
    @property()
    name: string = 'Aya';
    @property({
        type:Boolean,
            // converter: {
            //     fromAttribute: (value, type) => {
            //         // `value` is a string
            //         // Convert it to a value of type `type` and return it
            //     },
            //     toAttribute: (value, type) => {
            //         // `value` is of type `type`
            //         // Convert it to a string and return it
            //     }
            // },
            // attribute: true,
            // hasChanged(newVal: string, oldVal: string) {
            //     return newVal?.toLowerCase() !== oldVal?.toLowerCase();
            // },
         //  noAccessor: true, // Value of property "active" will reflect to attribute "active" prevent Lit from generating a property accessor that overwrites the superclass's defined accessor
            reflect: true  // Value of property "active" will reflect to attribute "active"

        },
    )
    active: Boolean = false;

    //setter and gettter
    private _prop = 0;

    set prop(val: number) {
        let oldVal = this._prop;
        this._prop = Math.floor(val);
        this.requestUpdate('prop', oldVal);
    }

    @property()
    get prop() { return this._prop; }

    override render() {
        return html`
            <h1>${this.myName}</h1>
            <p class="active">IS Active</p>
            <span  >Active: ${this.active}</span>

            <button @click="${() => {
                this.active = !this.active
            }}">Toggle Active
            </button>
            Hello from My Props ${this.name}
        `
    }
}