import {html, LitElement} from "lit";
import {customElement} from "lit/decorators.js";
import {ClockController} from "./clock-controller";
import {DualClockController} from "./dual-clock-controller";

@customElement('element-with-clock-controller')
class ElementWithClockController extends LitElement{
    // A reactive controller is an object associated with a host component, which implements one or more host lifecycle callbacks or interacts with its host
    clock = new ClockController(this,100);
    dualClock = new DualClockController(this,3000,10000);
    override render(){
        const currentTime=this.clock.value;
        const dualTime1=this.dualClock.time1;
        const dualTime2=this.dualClock.time2;
        return html`
          <h4>Time With Clock Controller</h4>
           <p> Current Time update every 100ms: ${timeFormat.format(currentTime)}</p>
          <h4>Time1 With Dual Clock Controller</h4>
          <p> Current Time update every 3000ms: ${timeFormat.format(dualTime1)}</p>
          <h4>Time2 With Dual Clock Controller</h4>
          <p> Current Time update every 10000ms: ${timeFormat.format(dualTime2)}</p>
          <p></p>
          
        `
    }
}

const timeFormat = new Intl.DateTimeFormat('en-US',{
    hour:'numeric',minute:'numeric',second:'numeric'
})