import {ReactiveController, ReactiveControllerHost} from "lit";
export class ClockController implements ReactiveController{
    host: ReactiveControllerHost;
    value = new Date();
    timeout:number;
    private _TimerID?:number = undefined;
    constructor(host:ReactiveControllerHost,timeout:number = 1000) {
        // Store a reference to the host
        this.host = host;
        // Register for lifecycle updates
        host.addController(this);
        //or =>> (this.host = host).addController(this);

        this.timeout = timeout;
    }
    hostConnected() {
        this._TimerID = setInterval(()=>{
            this.value = new Date();
            this.host.requestUpdate();
        },this.timeout)
    }
    hostDisconnected() {
        clearInterval(this._TimerID);
        this._TimerID = undefined;
    }

}