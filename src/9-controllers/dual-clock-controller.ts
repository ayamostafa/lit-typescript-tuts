import {ReactiveController, ReactiveControllerHost} from "lit";
import {ClockController} from "./clock-controller";

export class DualClockController implements ReactiveController {
    // Controllers can be composed of other controllers as well. To do this create a child controller and forward the host to it.
    private clock1: ClockController;
    private clock2: ClockController;

    constructor(host: ReactiveControllerHost, delay1: number, delay2: number) {
        this.clock1 = new ClockController(host, delay1);
        this.clock2 = new ClockController(host, delay2);
    }

    get time1() { return this.clock1.value; }
    get time2() { return this.clock2.value; }
}