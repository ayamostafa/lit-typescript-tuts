#The reactive controller lifecycle, defined in the ReactiveController interface, is a subset of the reactive update cycle. LitElement calls into any installed controllers during its lifecycle callbacks. These callbacks are optional.

##hostConnected():
- Called when the host is connected.
- Called after creating the renderRoot, so a shadow root will exist at this point.
Useful for setting up event listeners, observers, etc.
##hostUpdate():
- Called before the host's update() and render() methods.
Useful for reading DOM before it's updated (for example, for animations).
##hostUpdated():
- Called after updates, before the host's updated() method.
Useful for reading DOM after it's modified (for example, for animations).
##hostDisconnected():
- Called when the host is disconnected.
Useful for cleaning up things added in hostConnected(), such as event listeners and observers.