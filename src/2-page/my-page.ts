import {LitElement, html} from 'lit';
import {customElement} from 'lit/decorators.js';

import '../../scripts/page/partials/my-header.js';
import '../../scripts/page/partials/my-article.js';
import '../../scripts/page/partials/my-footer.js';

@customElement('my-2-page')
class MyPage extends LitElement {
   override render() {
        return html`
      <my-header></my-header>
      <my-article></my-article>
      <my-footer></my-footer>
    `;
    }
}
