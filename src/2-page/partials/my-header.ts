import {LitElement, html} from 'lit';
import {customElement} from 'lit/decorators.js';

@customElement('my-header')
class MyHeader extends LitElement {
  override  render() {
        return html`
      <header>header</header>
    `;
    }
}
